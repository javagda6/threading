package restaurant;

import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by amen on 11/30/17.
 */
public class Restaurant {

    private Executor workers = Executors.newFixedThreadPool(5);

    public void addNewOrder(){
        workers.execute(new Order());
        System.out.println("new Order arrived");
    }

}
