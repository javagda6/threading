package smsstation;

import java.util.ArrayList;
import java.util.Observable;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

/**
 * Created by amen on 11/30/17.
 */
public class SmsStation extends Observable implements ISmsSender{

    // Executor - tj. pula wątków (interfejs Executor)
    // Executors - tj. fabryka abstrakcyjna pul wątków. Wytwarza instancje
    // ThreadPoolExecutors
    private Executor anteny = Executors.newFixedThreadPool(5);

//    private List<Phone> phones = new ArrayList<>();

    public void addPhone(String number) {
//        phones.add(new Phone(number));
        addObserver(new Phone(number));
    }

    public void sendSmsRequest(String number, String content) {
        SmsRequest request = new SmsRequest(number, content, this);
//        for (Phone p : phones) {
//            request.addObserver(p);
//        }
        anteny.execute(request);
    }

    public void sendSms(Message request){
        setChanged();
        notifyObservers(request);
    }
}
