package smsstation;

/**
 * Created by amen on 11/30/17.
 */
public class Message {
    private String number, content;

    public Message(String number, String content) {
        this.number = number;
        this.content = content;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
