package smsstation;

/**
 * Created by amen on 11/30/17.
 */
public class Main {
    public static void main(String[] args) {
        SmsStation station = new SmsStation();

        station.addPhone("123");
        station.addPhone("124");
        station.addPhone("125");
        station.addPhone("126");
        station.addPhone("127");

        station.sendSmsRequest("122", "Poklikash?");
        station.sendSmsRequest("123", "Poklikash aaaaaaaaaa?");
        station.sendSmsRequest("124", "Poklikash aaaaaaaaaaaaa?");
        station.sendSmsRequest("124", "Poklikash aaa2?");
    }
}
