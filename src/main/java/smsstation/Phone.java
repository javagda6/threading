package smsstation;


import java.util.Observable;
import java.util.Observer;

/**
 * Created by amen on 11/30/17.
 */
public class Phone implements Observer {
    private String phoneNumber;

    public Phone(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    @Override
    public void update(Observable o, Object arg) {
        if (arg instanceof Message) {
            Message request = (Message) arg;
            // jeśli sms z requestu jest taki jak telefonu (czyli ten sms jest do mnie)
            if (request.getNumber().equalsIgnoreCase(getPhoneNumber())) {
                System.out.println(phoneNumber +" -> Otrzymalem sms: " + request.getContent());
            }
        }
    }
}
