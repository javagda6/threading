package smsstation;

import java.util.Observable;

/**
 * Created by amen on 11/30/17.
 */
public class SmsRequest /*extends Observable*/ implements Runnable {
    private ISmsSender station;
    private Message message;

    public SmsRequest(String number, String content, ISmsSender station) {
        this.message = new Message(number, content);
        this.station = station;
    }

    public void run() {
        try {
            //
            System.out.println("Rozpoczynam szyfrowanie wiadomości.");
            Thread.sleep(100 * message.getContent().length());
            System.out.println("Wysyłam wiadomość.");
            // wyslanie smsa

            // opcja 1:
//            setChanged();
//            notifyObservers(this);
            station.sendSms(message);

        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
