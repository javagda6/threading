package smsstation;

/**
 * Created by amen on 11/30/17.
 */
public interface ISmsSender {
    void sendSms(Message request);
}
